function string1(test_string) {
    let newString = ""
    for (let index = 0; index < test_string.length; index++) {
        if (test_string[index] === "-" || test_string[index] === ".") {
            newString += test_string[index]
        } else if (!isNaN(test_string[index])) {
            newString += test_string[index]
        }
    }
    return parseFloat(newString)
}
console.log(string1("-$7,908.90"))

module.exports = string1
