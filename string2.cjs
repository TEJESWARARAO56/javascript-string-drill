function string2(test_string) {
    let array_of_integers = test_string.split(".").map((string_number) => {
        for(let index=0;index<string_number.length;index++){
            if(isNaN(string_number[index])){
                return ""
            }
        }
        return parseInt(string_number)
    })
    if(array_of_integers.some((number)=>{
        return number===""
    })){
        return []
    }
    
    return array_of_integers
    
}
console.log(string2("111.139.161.143"))
module.exports = string2
