function string3(test_string) {
    let date = new Date(test_string)
    const month = date.toLocaleString("default", { month: 'long' });
    return month
}

console.log(string3("12/12/2021"))

module.exports = string3