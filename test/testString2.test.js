const string2 = require("../string2.cjs");
test("testing string2 function ", () => {
    let result = string2("101.102.103")
    expect(result).toStrictEqual([101, 102, 103])
    let result2 = string2("1y01.102.103")
    expect(result2).toStrictEqual([])
})