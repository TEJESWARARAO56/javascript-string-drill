const string4 = require("../string4.cjs");

test("testing full name function", () => {
    const result = string4({ "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" })
    expect(result).toStrictEqual("John Doe Smith")
});