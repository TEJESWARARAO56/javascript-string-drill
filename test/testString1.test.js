const string1 = require('../string1.cjs');

test('checking testString1 function', () => {
    const result = string1("-$4,506.89");
    expect(result).toStrictEqual(-4506.89);
});