const string3 = require('../string3.cjs');

test("testing get month function", () => {
    const result = string3("4/01/2023")
    expect(result).toStrictEqual("April")
})
