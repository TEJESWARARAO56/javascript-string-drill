const string5 = require('../string5.cjs');
test("testing array join method", () => {
    const result = string5(["the", "quick", "brown", "fox"])
    expect(result).toStrictEqual("the quick brown fox")
})