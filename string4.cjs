function string4(test_obj) {
    let title_string = ""
    for (let key in test_obj) {
        let name_part = test_obj[key].toLowerCase()
        title_string += " " + name_part.charAt(0).toUpperCase() + name_part.slice(1)
    }
    return title_string.slice(1)
}

console.log(string4({ "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" }))

module.exports = string4